package hello.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Table(name = "appuser")
@NoArgsConstructor
@ToString(exclude = { "authorities" }) //prevents stackoverflow exception when calling toString
public class User implements UserDetails {

    private static final long serialVersionUID = 3709949243021685681L;

    @Id
    @Column(name = "user_id", nullable = false, updatable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true, length = 20)
    @Size(min = 3, max = 20, message = "Username must be between 3 and 20 characters long")
    private String username;

    @Column(nullable = false)
    @Size(min = 4, max = 100, message = "Password must be at least 4 characters long")
    private String password;

    @Column(nullable = false, unique = true)
    @Email(message = "Incorrect email format")
    private String email;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "appuser_role",
            joinColumns = @JoinColumn(name = "appuser_id"),
            inverseJoinColumns = @JoinColumn(name = "approle_id"))
    private List<Role> authorities;

    @Column
    private boolean isAccountNonExpired;

    @Column
    private boolean isAccountNonLocked;

    @Column
    private boolean isCredentialsNonExpired;

    @Column
    private boolean isEnabled = true;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private Date creationDate;
}
