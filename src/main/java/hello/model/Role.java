package hello.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "approle")
@NoArgsConstructor
public class Role implements GrantedAuthority {

    private static final long serialVersionUID = 6410095741748080177L;

    private static final String PREFIX = "ROLE_";

    @Id
    @Column(name = "approle_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    @Column(unique = true, nullable = false)
    private String authority;

    @ManyToMany(mappedBy = "authorities")
    private Set<User> users = new HashSet<>(0);

    public Role(RoleEnum authority) {
        this.authority = PREFIX + authority.toString();
    }

    @Override
    public String toString() {
        return authority;
    }

}
