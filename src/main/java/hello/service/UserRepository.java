package hello.service;

import org.springframework.data.repository.CrudRepository;

import hello.model.User;

interface UserRepository extends CrudRepository<User, Long> {

    User findByEmail(String email);

    User findByUsername(String userName);

}
