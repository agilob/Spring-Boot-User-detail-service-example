package hello.service;

import hello.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByAuthority(String authority);

}
