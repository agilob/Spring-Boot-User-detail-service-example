package hello.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import hello.model.Role;
import hello.model.RoleEnum;
import hello.model.User;

@Service
@Transactional
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Long registerNewUser(User user) {

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        final Role USER_ROLE = roleService.findByAuthority(RoleEnum.USER);
        user.setAuthorities(Collections.singletonList(USER_ROLE));

        return this.save(user);
    }

    public Long save(User user) {
        try {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepo.save(user);
            return user.getId();
        } catch (Exception e) {
            LOGGER.error(e.toString(), e);
            return -1L;
        }
    }

    public Iterable<User> findAll() {
        return userRepo.findAll();
    }

    public User findByEmail(String email) {
        return userRepo.findByEmail(email);
    }

    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    public User findById(Long id) {
        return userRepo.findOne(id);
    }

}
