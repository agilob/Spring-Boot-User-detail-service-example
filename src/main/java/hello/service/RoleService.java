package hello.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.validation.constraints.NotNull;

import hello.model.Role;
import hello.model.RoleEnum;

@Service
@Transactional
public class RoleService {

    @Autowired
    private RoleRepository roleRepo;

    public List<Role> findAll() {
        return roleRepo.findAll();
    }

    private Role findByAuthority(@NotNull String authority) {
        return roleRepo.findByAuthority(authority);
    }

    public Role findByAuthority(@NotNull RoleEnum authority) {
        return roleRepo.findByAuthority(authority.toString());
    }

    public Role findOne(@NotNull Long id) {
        return roleRepo.findOne(id);
    }

    public Role save(@NotNull Role role) {
        return roleRepo.save(role);
    }

    public Role createRoleIfNotFound(RoleEnum roleEnum) {
        Role role = this.findByAuthority(roleEnum.toString());
        if (role == null) {
            role = new Role(roleEnum);
            this.save(role);
        }
        return role;
    }
}
