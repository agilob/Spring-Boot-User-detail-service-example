package hello.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class LoginController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping("/login")
    public ModelAndView login(final Principal principal) {
        if (principal == null) {
            // Login form
            LOGGER.debug("Unauthenticated");
            return new ModelAndView("login");
        }
        LOGGER.debug("Redirect to /");
        return new ModelAndView("redirect:/");
    }

}
