package hello.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController extends AbstractController {

    @GetMapping(value = "/")
    public String index() {
        return "index";
    }

    @RequestMapping("/thisOneThrowsExceptionToTestExceptionHandling")
    public void thisOneThrowsExceptionToTestExceptionHandling() {
        throw new RuntimeException("thisOneThrowsExceptionToTestExceptionHandling");
    }


}