package hello.web;

import hello.model.SignupUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class SignupController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignupController.class);

    @GetMapping(value = "/signup")
    public String showRegistrationForm(Model model) {
        model.addAttribute("newSingupUser", new SignupUser());
        return "signup";
    }

    @PostMapping(value = "/signup")
    public String registerUserAccount(
         @Valid @ModelAttribute("newSingupUser") SignupUser signupUser,
         BindingResult bindingResult, Model model) {

        boolean createUser = true;

        if(!signupUser.getPassword().equals(signupUser.getMatchingPassword())) {
            bindingResult.rejectValue("password", "error.password",
                    "Passwords don't match");
            bindingResult.rejectValue("matchingPassword", "error.matchingPassword",
                    "Passwords don't match");
            createUser = false;
        }

        if (userService.findByUsername(signupUser.getUsername()) != null) {
            bindingResult.rejectValue("username", "error.username",
                    "Username taken");
            model.addAttribute("user", signupUser);
            createUser = false;
        }

        if (userService.findByEmail(signupUser.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email",
                    "An account already exists for this email.");
            model.addAttribute("user", signupUser);
            createUser = false;
        }

        if (createUser) {
            userService.registerNewUser(signupUser.getAsUser());
            LOGGER.warn("User {} created", signupUser.getUsername());
        }

        if(bindingResult.hasErrors()) {
            return "signup";
        } else {
            return "login";
        }
    }

}
