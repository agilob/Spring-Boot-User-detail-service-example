package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

import hello.model.Role;
import hello.model.RoleEnum;
import hello.model.User;
import hello.service.RoleService;
import hello.service.UserService;
import lombok.extern.log4j.Log4j2;

@Log4j2
@EnableScheduling
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

    @Bean
    public CommandLineRunner demo() {
        return (args) -> {

            final Role r = roleService.createRoleIfNotFound(RoleEnum.ADMIN);
            roleService.createRoleIfNotFound(RoleEnum.USER);

            final User agilob = new User();
            agilob.setUsername("agilob");
            agilob.setEmail("agilob@agilob.net");
            agilob.setPassword("password1234");
            agilob.setAuthorities(Collections.singletonList(r));
            userService.save(agilob);

        };
    }

}
