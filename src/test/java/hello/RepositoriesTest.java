package hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.transaction.Transactional;

import hello.model.User;
import hello.service.UserService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestApplication.class, loader = AnnotationConfigContextLoader.class)
public class RepositoriesTest {

    private static final String USERNAME = "agilob";

    private static final String EMAIL = "agilob@agilob.net";

    private static final String PASSWORD = "password";

    @Autowired
    private UserService userService;

    @Test
    public void testCreatingUser() {
        final User u = new User();
        u.setEmail(EMAIL);
        u.setPassword(PASSWORD);
        u.setUsername(USERNAME);
        userService.save(u);

        final User u2 = userService.findByEmail(EMAIL);

        assertNotNull("User not found", u2);
        assertEquals("IDs don't match", u.getId(), u2.getId());
    }

    @Test
    public void testFindMethods() {
        final User u = new User();
        u.setEmail(EMAIL);
        u.setPassword(PASSWORD);
        u.setUsername(USERNAME);
        userService.save(u);

        User u2 = userService.findByEmail(EMAIL);

        assertNotNull("User not found", u2);
        assertEquals("IDs don't match", u.getId(), u2.getId());

        u2 = userService.findByUsername(USERNAME);
        assertNotNull("User not found", u2);
        assertEquals("IDs don't match", u.getId(), u2.getId());
    }

}
