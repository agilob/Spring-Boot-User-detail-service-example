package hello;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import javax.transaction.Transactional;

import hello.model.Role;
import hello.model.RoleEnum;
import hello.model.User;
import hello.service.RoleService;
import hello.service.UserService;

@SpringBootApplication
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations = "classpath:application.properties")
public abstract class TestApplication extends SpringBootServletInitializer {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Bean
    public CommandLineRunner demo() {
        return (args) -> {

            final User admin = new User();
            admin.setUsername("admin");
            admin.setEmail("admin@admin.net");
            admin.setPassword("password");
            admin.setAuthorities(Collections.singletonList(createRoleIfNotFound(RoleEnum.ADMIN)));
            userService.save(admin);


            final User user = new User();
            user.setUsername("user");
            user.setEmail("user@agilob.net");
            user.setPassword("user");
            user.setAuthorities(Collections.singletonList(createRoleIfNotFound(RoleEnum.USER)));
            userService.save(user);
        };
    }

    @Transactional
    private Role createRoleIfNotFound(RoleEnum roleEnum) {
        Role role = roleService.findByAuthority(roleEnum);
        if (role == null) {
            role = new Role(roleEnum);
            roleService.save(role);
        }
        return role;
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TestApplication.class);
    }

}
