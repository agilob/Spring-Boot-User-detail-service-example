package hello.web;

import hello.WebControllerTest;
import hello.model.User;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SignupControllerTest extends WebControllerTest {

    @Test
    public void userAlreadyExistsTest() throws Exception {
        mvc.perform(get("/signup"))
                .andExpect(status().isOk());

        final User newUser = userService.findByUsername("admin");
        assertNotNull("User found", newUser);

        //@formatter:off
        mvc.perform(post("/signup")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("username", "admin")
                    .param("password", "password")
                    .param("matchingPassword", "password")
                    .param("email", "admin@domain.com")
                    .with(csrf().asHeader()))
                .andExpect(status().isOk())
                .andExpect(view().name("signup"))
                .andExpect(model().hasErrors())
                .andExpect(unauthenticated());
        //formatter:on
    }

    @Test
    public void mismatchingPassword() throws Exception {
        mvc.perform(get("/signup"))
                .andExpect(status().isOk());

        //@formatter:off
        mvc.perform(post("/signup")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("username", "newUser")
                    .param("password", "password")
                    .param("matchingPassword", "padsadasdasssword")
                    .param("email", "admin@asdasda.com")
                    .with(csrf().asHeader()))
                .andExpect(status().isOk())
                .andExpect(view().name("signup"))
                .andExpect(model().hasErrors())
                .andExpect(unauthenticated());
        //formatter:on

        final User newUser = userService.findByUsername("newUser");
        assertNull("User found, should be not", newUser);
    }

    @Test
    public void takenEmail() throws Exception {
        mvc.perform(get("/signup"))
                .andExpect(status().isOk());

        //@formatter:off
        mvc.perform(post("/signup")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("username", "newUser")
                    .param("password", "password1234")
                    .param("matchingPassword", "password1234")
                    .param("email", "admin@admin.net")
                    .with(csrf().asHeader()))
                .andExpect(status().isOk())
                .andExpect(view().name("signup"))
                .andExpect(model().hasErrors())
                .andExpect(unauthenticated());
        //formatter:on

        final User newUser = userService.findByUsername("newUser");
        assertNull("User found, should be not", newUser);
    }

    @Test
    public void userSuccessfulSignup() throws Exception {
        //@formatter:off
        mvc.perform(post("/signup")
                    .param("username", "newuser233456")
                    .param("password", "newuser233456")
                    .param("matchingPassword", "newuser233456")
                    .param("email", "email@email2456.com")
                    .with(csrf().asHeader()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("login"))
                .andExpect(model().hasNoErrors())
                .andExpect(unauthenticated());
        //@formatter:on

        final User newUser = userService.findByUsername("newuser233456");
        assertNotNull("Newly created user not found", newUser);
    }
}
