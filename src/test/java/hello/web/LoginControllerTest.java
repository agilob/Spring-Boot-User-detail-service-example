package hello.web;

import hello.WebControllerTest;
import org.junit.Test;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LoginControllerTest extends WebControllerTest {

    @Test
    public void loginAsAdmin() throws Exception {
        final RequestBuilder requestBuilder = formLogin()
                .user("username")
                .password("passowrd");
        mvc.perform(
                formLogin("/login")
                        .user("admin")
                        .password("password"))
                .andExpect(status().is3xxRedirection())
                .andExpect(authenticated().withUsername("admin"));
    }

    @Test
    public void loginAsNonExisting() throws Exception {
        final RequestBuilder requestBuilder = formLogin()
                .user("username")
                .password("passowrd");
        mvc.perform(
                formLogin("/login")
                        .user("WHATISTHIS")
                        .password("ISTHISAPASSWORD"))
                .andExpect(status().is3xxRedirection())
                .andExpect(unauthenticated());
    }

}