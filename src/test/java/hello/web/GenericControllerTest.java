package hello.web;

import hello.WebControllerTest;
import org.junit.Test;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GenericControllerTest extends WebControllerTest {

    @Test
    public void testExceptionHandling() throws Exception {
        mvc.perform(get("/thisOneThrowsExceptionToTestExceptionHandling"))
                .andExpect(status().isInternalServerError())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("exception"))
                .andExpect(unauthenticated());
    }

    @Test
    public void testExceptionHandlingAsUser() throws Exception {
        mvc.perform(get("/thisOneThrowsExceptionToTestExceptionHandling").with(user(USER)))
                .andExpect(status().isInternalServerError())
                .andExpect(view().name("error"))
                .andExpect(model().attributeExists("exception"))
                .andExpect(authenticated());
    }

}